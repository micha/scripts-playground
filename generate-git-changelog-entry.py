#!/usr/bin/env python3
import click
import os
import subprocess
import sys
import time

from debian.deb822 import Deb822
from debian.changelog import Changelog, ChangeBlock

DEBIAN_CHANGELOG = 'debian/changelog'
DEBIAN_CONTROL = 'debian/control'


def get_control(control_filename):
    with open(control_filename) as control_fh:
        return Deb822(control_fh)


def get_changelog(changelog_filename):
    with open(changelog_filename) as changelog_fh:
        return Changelog(changelog_fh)


class GitInfo():
    def __init__(self, gitdir=None):
        self.options = []
        if gitdir:
            self.options = ['-C', os.path.dirname(gitdir)]
        self._parse_status()
        self._parse_tag()

    def _git(self, *args, check=True):
        command = ['git', *self.options, *args]
        result = subprocess.run(command, capture_output=True, check=check)
        return result.stdout.decode('UTF-8')

    def _parse_status(self):
        self.dirty = False
        self.commit = None
        self.branch = None

        output = self._git('status', '--porcelain=v2', '--branch')
        for line in output.rstrip(os.linesep).split(os.linesep):
            if line[0:2] == '# ':
                key, value = line[2:].split(' ', 1)
                if key == 'branch.oid':
                    self.commit = value
                elif key == 'branch.head':
                    self.branch = value
            else:
                if line:
                    self.dirty = True

    def _parse_tag(self):
        output = self._git('describe', '--tags', '--abbrev=0', check=False)
        self.last_tag = output.rstrip(os.linesep)

    def gen_version(self, timestamp=None):
        if timestamp is None:
            timestamp = int(time.time())
        dirty_option = '--dirty=+dirty' + str(timestamp)
        version = self._git('describe', '--tags', '--long', dirty_option)
        return version.replace('-', '.').rstrip(os.linesep)

    def get_debian_changelog(self):
        output = self._git('log', '--format=  * %h %s',
                           self.last_tag + '..HEAD')
        return output.rstrip(os.linesep).split(os.linesep)

    def last_change(self):
        output = self._git('log', '-n', '1', '--format=%cD', 'HEAD')
        return output.rstrip(os.linesep)


@click.command()
@click.option('--git-dir', envvar='GBP_GIT_DIR')
@click.option('--verbose', '-v', is_flag=True)
def cli(git_dir, verbose):
    control = get_control(DEBIAN_CONTROL)
    changelog = get_changelog(DEBIAN_CHANGELOG)
    git = GitInfo(git_dir)
    entry = ChangeBlock(
        package=control['Source'],
        version=git.gen_version(),
        distributions=git.branch,
        urgency='medium',
        author=control['Maintainer'],
        date=git.last_change())
    entry.add_change('')
    for line in git.get_debian_changelog():
        entry.add_change(line)
    entry.add_change('')
    if verbose:
        print("Generated debian/changelog entry:")
        print(entry)
    with open(DEBIAN_CHANGELOG, 'w') as changelog_fh:
        print(entry, file=changelog_fh)
        print(changelog, file=changelog_fh)


if __name__ == '__main__':
    cli()
